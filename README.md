# README #

This is for Pooled Sequencing Toolkit (PeSTo)'s home. The aim is to bring together as many poolseq algorithms in an efficient package to allow fast and parallel analyses. 

### Who do I talk to? ###

* Please contact Rahul Rane [Rahul(dot)Rane(at)csiro(dot)au] or Heng Lin Yeap [HengLin(dot)Yeap(at)csiro(dot)au]
